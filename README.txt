Provides Granular Permissions for Paragraphs entities enables you to assign permissions per Paragraph entity. You will need https://www.drupal.org/project/paragraphs to use this module.

Paragraphs is developed and maintained by New Target Inc. www.newtarget.com