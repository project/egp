<?php

namespace Drupal\egp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

class EGPForm extends FormBase {

  public function getFormId() {
    return 'egp_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $nid = $_GET['nid'];
    $pid = $_GET['pid'];
    $type = $_GET['type'];

    $roles = _egp_default_roles();

    foreach($roles as $key => $value) {
      if($key == 'administrator') {
        continue;
      }
      $connection = \Drupal::database();
      $results = $connection
        ->query(
          'SELECT * FROM {testing_perms} WHERE pid = :pid AND nid = :nid AND role = :role',
          [
            ':pid' => (int) $pid,
            ':nid' => (int) $nid,
            ':role' => $key
          ]
        )
        ->fetchAll();
      if(count($results) > 0) {
        foreach($results as $record) {
          $value['permissions'][$type] = $record->access;
        }
      }

      $form['role_' . $key . '-' . $nid . '-' . $pid] = [
        '#type' => 'checkbox',
        '#title' => $this->t($value['label']),
        '#default_value' => !empty($value['permissions'][$type]) ? TRUE : '',
        '#attributes' =>
          [
            'class' => ['permissions-inline']
          ]
      ];
    }

    $form['actions'] = [
      '#type' => 'actions'
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Permissions'),
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    _egp_save_new_permissions($values);
    drupal_flush_all_caches();
  }
}
